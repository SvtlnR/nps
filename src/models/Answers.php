<?php

namespace nps\models;

use \Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    protected $table = 'nps_answers';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'npsId',
        'createdAt',
        'mark'
    ];
}