<?php

namespace nps\models;

use \Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $table = 'nps_questions';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'title',
        'timeout',
        'rangemark',
        'visible',
        'typeview',
        'createdAt'
    ];
}