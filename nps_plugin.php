<?php

/*Plugin Name: nps_plugin
*/
mb_internal_encoding("UTF-8");

use Illuminate\Database\Capsule\Manager as DB;

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'nps_functions.php';
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection([
    'driver' => 'mysql',
    'host' => DB_HOST,
    'database' => DB_NAME,
    'username' => DB_USER,
    'password' => DB_PASSWORD,
    'charset' => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix' => '',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();
register_activation_hook(__FILE__, 'activate_nps');

if (isset($_REQUEST['action']) && !isset($_REQUEST['type'])) {
    do_action('wp_ajax_nopriv_' . $_REQUEST['action']);
    do_action('wp_ajax_' . $_REQUEST['action']);
}

add_action('wp_head', 'myplugin_ajaxurl');
function myplugin_ajaxurl()
{
    echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}

add_action('wp_head', 'frontend_js_wp_head');
function frontend_js_wp_head()
{

    echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">';
    include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'npsPopup.php';
    echo '<script>';
    include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'npsPopupScript.js';
    echo '</script>';

}

add_action('wp_ajax_nopriv_nps_export_marks', 'exportMarks');
add_action('wp_ajax_nps_export_marks', 'exportMarks');

function exportMarks()
{

    $marksValues = isset($_REQUEST['marksValues'])?$_REQUEST['marksValues']:null;
    $marksLabels= isset($_REQUEST['marksLabels'])?$_REQUEST['marksLabels']:null;

    if ($marksValues === null || $marksLabels === null) {
        echo json_encode([
            'status' => false,
            'line' => __LINE__,
            'body' => [
                'errorMessage' => 'Bad Params'
            ]
        ]);
        die();
    }

    if (!file_exists('nps_exports')) {
        mkdir('nps_exports', 0777, true);
    }
    $csvFileMarks=new \nps\utils\CsvFile("nps_exports/nps_marks_per_day.csv");
    $csvFileMarks->openFile();
    $csvFileMarks->addToFile(
        array(
            "mark",
            "amount"
        )
    );
    for($i=0;$i<count($marksLabels);$i++){
        $values = array (
            $marksLabels[$i],
            $marksValues[$i]
        );
        $csvFileMarks->addToFile($values);
    }
    $csvFileMarks->closeFile();
    echo json_encode([
        'status' => true,
        'line' => __LINE__,
        'body' => [
            'message' => 'Data Export Success'
        ]
    ]);
    wp_die();
    die();

}

add_action('wp_ajax_nopriv_nps_export_data', 'exportData');
add_action('wp_ajax_nps_export_data', 'exportData');

function exportData()
{
    $idExport = isset($_REQUEST['idExport'])?$_REQUEST['idExport']:null;
    $questMarksLabels= isset($_REQUEST['questMarksLabels'])?$_REQUEST['questMarksLabels']:null;
    $questPercent= isset($_REQUEST['questPercent'])?$_REQUEST['questPercent']:null;
    $questAmount= isset($_REQUEST['questAmount'])?$_REQUEST['questAmount']:null;
    if ($idExport === null || $questMarksLabels === null||$questPercent === null || $questAmount === null) {
        echo json_encode([
            'status' => false,
            'line' => __LINE__,
            'body' => [
                'errorMessage' => 'Bad Params'
            ]
        ]);
        die();
    }
    $modelQuestion = \nps\models\Questions::query()
        ->where('id', '=', $idExport)
        ->first();

    if ($modelQuestion === null) {
        echo json_encode([
            'status' => false,
            'line' => __LINE__,
            'body' => [
                'errorMessage' => 'Nps Question Not Found'
            ]
        ]);
        die();
    }
    if (!file_exists('nps_exports')) {
        mkdir('nps_exports', 0777, true);
    }

    $csvFileMarks=new \nps\utils\CsvFile("nps_exports/nps_dataQuestion".$idExport.".csv");
    $csvFileMarks->openFile();
    $csvFileMarks->addToFile(
        array(
            "id",
            "title",
            "maxMark",
            "Mark",
            "Percent",
            "AmountOfAnswers",
            "createdAt"
        )
    );
    for($i=0;$i<count($questMarksLabels);$i++){
        $values = array (
            $modelQuestion->id,
            $modelQuestion->title,
            $modelQuestion->rangemark,
            $questMarksLabels[$i],
            $questPercent[$i],
            $questAmount[$i],
            $modelQuestion->createdAt
        );
        $csvFileMarks->addToFile($values);
    }
    $csvFileMarks->closeFile();
    echo json_encode([
        'status' => true,
        'line' => __LINE__,
        'body' => [
            'message' => 'Data Export Success'
        ]
    ]);
    wp_die();
    die();

}

add_action('wp_ajax_nopriv_nps_export_amount', 'exportAmount');
add_action('wp_ajax_nps_export_amount', 'exportAmount');

function exportAmount()
{
    $amountValues = isset($_REQUEST['amountValues'])?$_REQUEST['amountValues']:null;
    $dayLabels= isset($_REQUEST['dayLabels'])?$_REQUEST['dayLabels']:null;

    if ($amountValues === null || $dayLabels === null) {
        echo json_encode([
            'status' => false,
            'line' => __LINE__,
            'body' => [
                'errorMessage' => 'Bad Params'
            ]
        ]);
        die();
    }

    if (!file_exists('nps_exports')) {
        mkdir('nps_exports', 0777, true);
    }
    $csvFileAmount=new \nps\utils\CsvFile("nps_exports/nps_amount_per_day.csv");
    $csvFileAmount->openFile();
    $csvFileAmount->addToFile(
        array(
            "date",
            "amount"
        )
    );
    for($i=0;$i<count($dayLabels);$i++){
        $values = array (
            $dayLabels[$i],
            $amountValues[$i]
        );
        $csvFileAmount->addToFile($values);
    }
    $csvFileAmount->closeFile();
    echo json_encode([
        'status' => true,
        'line' => __LINE__,
        'body' => [
            'message' => 'Data Export Success'
        ]
    ]);
    wp_die();
    die();

}

add_action('wp_ajax_nopriv_nps_delete_question', 'deleteQuest');
add_action('wp_ajax_nps_delete_question', 'deleteQuest');
add_action('wp_ajax_nopriv_nps_update_question', 'updateQuest');
add_action('wp_ajax_nps_update_question', 'updateQuest');



add_action('wp_ajax_nopriv_nps_add_dateClose', 'addCloseDate');
add_action('wp_ajax_nps_add_dateClose', 'addCloseDate');

function addCloseDate(){
    $cookieName='nps_dateClose';
    $dateClose=isset($_REQUEST['closeTime']) ? $_REQUEST['closeTime'] : null;
    if($dateClose===null){
        echo json_encode([
            'status' => false,
            'line' => __LINE__,
            'body' => [
                'errorMessage' => 'Bad Params'
            ]
        ]);
        die();
    }
    setcookie($cookieName, $dateClose, time() + 31536000, '/', COOKIE_DOMAIN);
    $_COOKIE[$cookieName] = $dateClose;

    echo json_encode([
        'status' => true,
        'line' => __LINE__,
        'body' => [
            'message' => 'Closed Successfully'
        ]
    ]);
    die();
}
//
add_action('wp_ajax_nopriv_nps_add_dateSuccess', 'addDateSuccess');
add_action('wp_ajax_nps_add_dateSuccess', 'addDateSuccess');

function addDateSuccess(){
    $cookieName='nps_dateSend';
    $dateSend=isset($_REQUEST['dateSend']) ? $_REQUEST['dateSend'] : null;
    if($dateSend===null){
        echo json_encode([
            'status' => false,
            'line' => __LINE__,
            'body' => [
                'errorMessage' => 'Bad Params'
            ]
        ]);
        die();
    }
    setcookie($cookieName, $dateSend, time() + 31536000, '/', COOKIE_DOMAIN);
    $_COOKIE[$cookieName] = $dateSend;

    echo json_encode([
        'status' => true,
        'line' => __LINE__,
        'body' => [
            'message' => 'SuccessTime'
        ]
    ]);
    die();

}



add_action('wp_ajax_nopriv_nps_add_answer', 'addAnswer');
add_action('wp_ajax_nps_add_answer', 'addAnswer');
function addAnswer()
{   $cookieName = 'nps_usedId';
    $idQuest = isset($_REQUEST['idQuest']) ? $_REQUEST['idQuest'] : null;
    $mark = isset($_REQUEST['mark']) ? $_REQUEST['mark'] : null;

    if ($idQuest === null || $mark === null) {
        echo json_encode([
            'status' => false,
            'line' => __LINE__,
            'body' => [
                'errorMessage' => 'Bad Params'
            ]
        ]);
        die();
    }

    $mark = (int)$mark;
    $idQuest = (int)$idQuest;

    // check question
    $modelQuestion = \nps\models\Questions::query()
        ->where('id', '=', $idQuest)
        ->first();

    if ($modelQuestion === null) {
        echo json_encode([
            'status' => false,
            'line' => __LINE__,
            'body' => [
                'errorMessage' => 'Nps Question Not Found'
            ]
        ]);
        die();
    }

    // save Answer
    $answ = new \nps\models\Answers();
    $answ->npsId = $modelQuestion->id;
    $answ->mark = $mark;
    $answ->save();


    $lisViewId = isset($_COOKIE[$cookieName]) ? json_decode($_COOKIE[$cookieName], true) : [];
    $lisViewId = !is_array($lisViewId) ? [] : array_unique($lisViewId);
    $lisViewId[] = $modelQuestion->id;
    setcookie($cookieName, json_encode($lisViewId), time() + 31536000, '/', COOKIE_DOMAIN);
    $_COOKIE[$cookieName] = $lisViewId;

    echo json_encode([
        'status' => true,
        'line' => __LINE__,
        'body' => [
            'message' => 'Thank'
        ]
    ]);

//    $cookieName='nps_dateSend';
//    $dateSend=isset($_REQUEST['dateSend']) ? $_REQUEST['dateSend'] : null;
//    if($dateSend===null){
//        echo json_encode([
//            'status' => false,
//            'line' => __LINE__,
//            'body' => [
//                'errorMessage' => 'Bad Params'
//            ]
//        ]);
//        die();
//    }
//    setcookie($cookieName, $dateSend, time() + 31536000, '/', COOKIE_DOMAIN);
//    $_COOKIE[$cookieName] = $dateSend;
//
//    echo json_encode([
//        'status' => true,
//        'line' => __LINE__,
//        'body' => [
//            'message' => 'SuccessTime'
//        ]
//    ]);
//

    die();
}

function deleteQuest()
{
    $id = $_REQUEST['id'];
    $quest = \nps\models\Questions::query()
        ->where('id', '=', $id)
        ->delete();
    wp_die();
    die();

}

function updateQuest()
{
    $id = $_REQUEST['id'];
    $title = $_REQUEST['title'];
    $timeout = $_REQUEST['timeout'];
    $range = $_REQUEST['range'];
    $visible = $_REQUEST['visible'];
    $typeview=($_REQUEST['typeview']==='popup')?1:0;
    $updated_quest = \nps\models\Questions::query()
        ->where('id', '=', $id)
        ->first();
    $updated_quest->title = $title;
    $updated_quest->timeout = $timeout;
    $updated_quest->rangemark = $range;
    $updated_quest->visible = $visible;
    $updated_quest->typeview = $typeview;
    $updated_quest->save();
}




add_shortcode( 'npsBlock', 'npsblock_func' );


function npsblock_func($atts){
    $atts = shortcode_atts( array(
        'id' => null
    ), $atts, 'npsBlock' );
    ob_start();
    $query = \nps\models\Questions::query()
        ->where('visible', '=', 1)
        ->where('typeview','=',0);
    if($atts['id']===null) {
        if (isset($_COOKIE['nps_usedId'])) {
            $lisViewId = json_decode($_COOKIE['nps_usedId'], true);
        }
        $lisViewId = !is_array($lisViewId) ? [] : array_unique($lisViewId);
        if (count($lisViewId) > 0) {
            $query->whereNotIn('id', $lisViewId);
        }
        $question = $query->first();
    }
    else{
        $question = $query
            ->where('id','=',$atts['id'])
            ->first();
    }
    if ($question === null) {
        return false;
    }

    echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">';
    include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'npsBlock.php';
    echo '<script>';
    include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'npsBlockScript.js';
    echo '</script>';
    ?><script>console.log(<?php echo $$atts['id']?>)</script><?php
    $content = ob_get_clean();
    return $content;
}

function activate_nps()
{
    DB::statement('CREATE TABLE IF NOT EXISTS nps_questions (
id INT AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(30) NOT NULL,
timeout INT NOT NULL,
rangemark INT NOT NULL,
createdAt TIMESTAMP
)');
    DB::statement('CREATE TABLE IF NOT EXISTS nps_answers (
id INT AUTO_INCREMENT PRIMARY KEY,
npsId INT NOT NULL,
createdAt TIMESTAMP,
mark INT NOT NULL
)');
}



