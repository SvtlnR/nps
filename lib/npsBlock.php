



<style>
    .nps_block {
        background-color:  #F5F5F5;
        color:  #696969;
        padding: 2%;
        margin: 1%;
    }

    #idQuest {
        display: none;
    }

    .nps_block_success {
        display: none;
        background-color:  #F5F5F5;
        color:  #696969;
        display: none;
        padding: 2%;
        margin: 1%;
    }
</style>
<div class="nps_block">
    <div class="nps_block_content">
        <div id="idQuest"><?php echo $question->id ?></div>
        <h4><?php echo $question->title ?></h4>
        <div class="container">
            <form id="blockForm">
                <div class="row">
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <?php for ($i = 1; $i <= $question->rangemark; $i++) : ?>
                            <label class="btn btn-outline-secondary labelsNps" for="radioNps_<?php echo $i ?>">
                                <input type="radio" name="radioNpsMark"  class="npsRadio"  autocomplete="off" value="<?php echo $i ?>">
                                <?php echo $i ?>
                            </label>
                        <?php endfor; ?>
                    </div>
                </div>
            </form>
        </div>
        <div class="container my-2">
            <button id='sendNpsBlock' class='btn btn-default'>Send</button>
            <button id='closeNpsBlock' class='btn btn-default'>Close</button>
        </div>
    </div>
</div>
<div class="nps_block_success">
    <h4>Thanks for answer!</h4>
    <button id='closeNpsSuccessBlock' class='btn btn-default'>Close</button>
</div>