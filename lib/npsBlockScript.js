jQuery(function ($) {
        $("#closeNpsBlock").click(function(){
            var currentTime=new Date().getTime();
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data:{
                    action: 'nps_add_dateClose',
                    closeTime: currentTime,
                },
                success: function() {
                    $(".nps_block").fadeOut('fast');
                }
            });

        });
        $("#closeNpsSuccessBlock").click(function() {
                    $(".nps_block_success").fadeOut('fast');

            });



        $(".labelsNps").click(function () {
            $(".labelsNps").removeClass('active');
            $(this).addClass('active');
            $(this).find('input[name=radioNpsMark]').prop('checked', true);
        });
        $("#sendNpsBlock").click(function() {
            var mark=$('input[name=radioNpsMark]:checked', '#blockForm').val();
            var timeSuccess = new Date().getTime();
            if(mark!==undefined) {
                var questId = $("#idQuest").text();
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data:{
                        action: 'nps_add_answer',
                        idQuest: questId,
                        mark: mark,
                    },
                    success: function() {
                        $(".nps_block").hide('fast');
                        $(".nps_block_success").fadeIn('fast');
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: {
                        action: 'nps_add_dateSuccess',
                        dateSend: timeSuccess
                    }
                });
            }
        });

    });
