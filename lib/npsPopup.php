<?php
if (isset($_COOKIE['nps_usedId']) /*||!isset($_COOKIE['nps_dateSend'])||!isset($_COOKIE['nps_dateClose'])*/) {
    $lisViewId = json_decode($_COOKIE['nps_usedId'], true);
}

$lisViewId = !is_array($lisViewId) ? [] : array_unique($lisViewId);
$query = \nps\models\Questions::query()
    ->where('visible', '=', 1)
    ->where('typeview', '=', 1);
if (count($lisViewId) > 0) {
    $query->whereNotIn('id', $lisViewId);
}
$question = $query->first();

if ($question === null) {
    return false;
}
$nextTime=$question->timeout;
if(isset($_COOKIE['nps_dateSend'])&&isset($_COOKIE['nps_dateClose'])){
    $nextTime=($_COOKIE['nps_dateSend']<$_COOKIE['nps_dateClose'])?86400:$question->timeout;
}
else if(isset($_COOKIE['nps_dateClose'])){
    $nextTime=86400;

}


?>
<script>
    window = window || {};
    window.__app__ = window.__app__ || {};
    window.__app__.npsPopup = window.__app__.npsPopup || {};
    window.__app__.npsPopup.id = +"<?php echo $question->id;?>";
    window.__app__.npsPopup.timeout = +"<?php echo $nextTime;?> ";
    console.log("script:<?php echo $question->timeout;?>");
    console.log(window.__app__.npsPopup.timeout);
</script>
<style>
    .nps_pop_overlay {
        background-color:  #F5F5F5;
        border-radius: 3px;
        color:  #696969;
        display: none;
        padding: 2%;
        margin: 1%;
        z-index: 99;
        position: fixed;
    }

    #idQuest {
        display: none;
    }

    .nps_pop_success {
        display: none;
        background-color:  #F5F5F5;
        border-radius: 3px;
        color:  #696969;
        padding: 2%;
        margin: 1%;
        z-index: 99;
        position: fixed;
    }
</style>
<div class="nps_pop_overlay">
    <div class="nps_pop_content">
        <div id="idQuest"><?php echo $question->id ?></div>
        <h4><?php echo $question->title ?></h4>
        <div class="container">
            <form id="popUpForm">
                <div class="row">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <?php for ($i = 1; $i <= $question->rangemark; $i++) : ?>
                    <label class="btn btn-outline-secondary labelsNps" for="radioNps_<?php echo $i ?>">
                        <input type="radio" name="radioNpsMark"  class="npsRadio"  autocomplete="off" value="<?php echo $i ?>">
                        <?php echo $i ?>
                    </label>
                    <?php endfor; ?>
                </div>
                </div>
            </form>
        </div>
        <div class="container my-2">
        <button id='sendNpsPopup' class='btn btn-default'>Send</button>
        <button id='closeNpsPopup' class='btn btn-default'>Close</button>
    </div>
    </div>
</div>
<div class="nps_pop_success">
    <h4>Thanks for answer!</h4>
    <button id='closeNpsSuccess' class='btn btn-default'>Close</button>
</div>