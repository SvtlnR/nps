jQuery(function ($) {
    $(window).load(function () {
        setTimeout(function () {
            $(".nps_pop_success").fadeOut('fast');
            $(".nps_pop_overlay").fadeIn('slow');
        },window.__app__.npsPopup.timeout  * 1000);

        $("#closeNpsPopup").click(function(){
            var currentTime=new Date().getTime();
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data:{
                    action: 'nps_add_dateClose',
                    closeTime: currentTime,
                },
                success: function() {
                    $(".nps_pop_overlay").fadeOut('fast');
                }
            });

        });


        $("#closeNpsSuccess").click(function() {
            $(".nps_pop_success").fadeOut('fast');
        });



            // $("#closeNpsSuccess").click(function() {
        //     var timeSuccess = new Date().getTime();
        //     $.ajax({
        //         type: 'POST',
        //         url: ajaxurl,
        //         data: {
        //             action: 'nps_add_dateSuccess',
        //             dateSend: timeSuccess
        //         },
        //         success: function () {
        //             $(".nps_pop_success").fadeOut('fast');
        //         }
        //     });
        // });


        $(".labelsNps").click(function () {
            $(".labelsNps").removeClass('active');
            $(this).addClass('active');
            $(this).find('input[name=radioNpsMark]').prop('checked', true);
        });
        $("#sendNpsPopup").click(function() {
            var mark=$('input[name=radioNpsMark]:checked', '#popUpForm').val();
            var timeSuccess = new Date().getTime();
            if(mark!==undefined) {
                var questId = $("#idQuest").text();
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data:{
                        action: 'nps_add_answer',
                        idQuest: questId,
                        mark: mark,
                    },
                    success: function() {
                        $(".nps_pop_overlay").fadeOut('fast');
                        $(".nps_pop_success").fadeIn('fast');
                    }
                });
                $.ajax({
                            type: 'POST',
                            url: ajaxurl,
                            data: {
                                action: 'nps_add_dateSuccess',
                                dateSend: timeSuccess
                            }
                        });
            }
        });
  });

});