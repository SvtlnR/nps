jQuery(document).ready(function($) {
    $('#questions').on('click', '#deleteQuestion', function() {
        var currentRow = $(this).closest("tr");
        var currentId = currentRow.find("th").text();
        $(".confirmDelete").fadeOut('fast');
        $("#confirmDelete_"+currentId).fadeIn('fast');
    });
    $(".confirmDeleteYes").click(function () {
        var currentId=$(this).attr('id');
        currentId=currentId.substring(currentId.indexOf('_')+1,currentId.length);
        $("#confirmDelete_"+currentId).fadeOut('fast');
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data:{
                action: 'nps_delete_question',
                id: currentId,
            },
            success: function() {
                location.reload();
            }
        });
    });
    $(".confirmDeleteNo").click(function () {
        var currentId=$(this).attr('id');
        currentId=currentId.substring(currentId.indexOf('_')+1,currentId.length);
        $("#confirmDelete_"+currentId).fadeOut('fast');
    });



    $('#questions').on('click', '#updateQuestion', function() {
        var currentRow = $(this).closest("tr");
        var currentId = currentRow.find("th").text();
        var currentTitle = currentRow.find("td.quest_title input").val();
        var currentTimeout = currentRow.find("td.quest_timeout input").val();
        var currentRangemark = currentRow.find("td.quest_rangemark input").val();
        var currentVisibility = currentRow.find("td.quest_visible input").prop('checked')?1:0;
        var currentTypeview = currentRow.find("td.quest_typeview option:selected").val();
        console.log(currentTypeview);
        $(currentRow).addClass("updatedRow");
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data:{
                action: 'nps_update_question',
                id:currentId,
                title:currentTitle,
                timeout:currentTimeout,
                range:currentRangemark,
                visible: currentVisibility,
                typeview: currentTypeview
            },
            success: function() {
                location.reload();
            }
        });

    });
    $('#questions').on('click', '#detailsQuestion', function() {
        var currentRow = $(this).closest("tr");
        var currentId = currentRow.find("th").text();
        var url=$(".admin_url").text()+'admin.php?page=nps_plugin%2Fincludes%2Fnps-questions-page.php&details='+currentId;
        console.log(url);
        console.log(currentId);
        $.ajax({
            type: 'GET',
            data:{
                url: url,
            },
            success: function() {
                document.location.href = url;
            }
        });

    });

    var idExport=$(".titleExport").text();
    idExport=idExport.substring(idExport.indexOf('#')+1, idExport.indexOf(':'));

    var questMarksLabels=new Array();
    $(".questionMarkDetails").each(function(){
        var label=parseInt($(this).text());
        questMarksLabels.push(label);
    });

    var questPercent=new Array();
    $(".questionPercentDetails").each(function(){
        var value=$(this).text();
        questPercent.push(value);
    });

    var questAmount=new Array();
    $(".questionAmountDetails").each(function(){
        var value=parseInt($(this).text());
        questAmount.push(value);
    });

    $('#exportData').click( function() {
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data:{
                action: 'nps_export_data',
                idExport: idExport,
                questMarksLabels: questMarksLabels,
                questPercent:questPercent,
                questAmount:questAmount
            }
        });
    });
});
