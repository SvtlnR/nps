<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="wrapper container">
    <div class="admin_url" style="display: none"><?php echo get_admin_url() ?></div>
    <?php if(!isset($_GET['graph'])&&!isset($_GET['filter'])){ ?>
    <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Answers</li>
                    <li class="breadcrumb-item"><a href="<?php echo get_admin_url()?>admin.php?page=nps_plugin%2Fincludes%2Fnps-answers-page.php&graph=1">Graphs</a></li>
                </ol>
            </nav>
    </div>
        <div class="row">
            <h2>Answers</h2>
        </div>
    <div class="row">
            <div class="col"><input type="text" class="form-control my-2" id="filterId" size="3" placeholder="Id"></div>
            <div class="col"><input type="text" class="form-control my-2" id="filterNpsId" size="3" placeholder="NpsId"></div>
            <div class="col"><input type="text" class="form-control my-2" id="filterDateFrom" size="10" placeholder="From"></div>
            <div class="col"><input type="text" class="form-control my-2" id="filterDateTo" size="10" placeholder="To"></div>
            <div class="col"><input type="text" class="form-control my-2" id="filterMark" size="2" placeholder="Mark"></div>
            <div class="col"><input type="submit" id="filter" value="Filter" class="my-2 btn btn-outline-secondary"></div>
    </div>
    <table id="answers" class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>npsId</th>
            <th>CreatedAt</th>
            <th>Mark</th>
        </tr>
        </thead>
        <tbody>
        <?php
            $answers=\nps\models\Answers::query()
                ->get();
            foreach($answers as $answer) :
                ?>
                <tr>
                    <th scope='row'><?php echo $answer->id ?></th>
                    <td><?php echo $answer->npsId ?></td>
                    <td><?php echo $answer->createdAt ?></td>
                    <td><?php echo $answer->mark ?></td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
    <?php
    }
    else if(isset($_GET['graph'])){
    ?>
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo get_admin_url()?>admin.php?page=nps_plugin%2Fincludes%2Fnps-answers-page.php">Answers</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Graphs</li>
                </ol>
            </nav>
        </div>
        <h2>
           Amount of Answers per Day/Amount of Marks
        </h2>
        <div class="graphAmountAnswers">
            <div class="row">
            <div class="col"> <canvas id="graphAnswersAmount" height="200"></canvas></div>
                <div class="col"><canvas id="graphAnswersMarks" height="200"></canvas></div>
            </div>
            <div class="my-2 row">
                <div class="col feature text-center">
                    <button id='exportAmountPerDay' class="btn btn-outline-secondary">Export to CSV</button>
                </div>
                <div class="col feature text-center">
                    <button id='exportMarksPerDay' class="btn btn-outline-secondary">Export to CSV</button>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <table id="answersPerDayAmount" class="table">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $answers=\nps\models\Answers::query()
                            ->get();
                        $datesAnswers=[];//all dates
                        $amountDateAnswer=[];//amount per day

                        $rangeMarks=[];//all marks
                        $marksAnswer=[];//amount of selected marks
                        foreach ($answers as $answer) {
                            $dateAnswer=substr($answer->createdAt,0,mb_strpos($answer->createdAt,' '));
                            if(!in_array($dateAnswer,$datesAnswers)){
                                array_push($datesAnswers,$dateAnswer);
                            }
                            $amountDateAnswer[$dateAnswer]++;
                            $mark=$answer->mark;
                            if(!in_array($mark,$rangeMarks)){
                                array_push($rangeMarks,$mark);
                            }
                            $marksAnswer[$mark]++;
                        }
                        foreach ($datesAnswers as $dateAnswer):
                            ?>
                            <tr>
                                <td>
                                    <div class="answerPerDayAmountLabel"><?php echo $dateAnswer?></div>
                                </td>
                                <td><div class="answerPerDayAmountValue"><?php echo $amountDateAnswer[$dateAnswer]?></div></td>
                            </tr>
                        <? endforeach;?>
                        </tbody>
                    </table>
                </div>
                <div class="col"></div>
                <div class="col">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Mark</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        asort($rangeMarks);
                        foreach ($rangeMarks as $mark):
                            ?>
                            <tr>
                                <td>
                                    <div class="answerMarkAmountLabel"><?php echo $mark?></div>
                                </td>
                                <td><div class="answerMarkAmountValue"><?php echo $marksAnswer[$mark]?></div></td>
                            </tr>
                        <? endforeach;?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    <?php }
    else if(isset($_GET['filter'])){?>
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo get_admin_url()?>admin.php?page=nps_plugin%2Fincludes%2Fnps-answers-page.php">Answers</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo get_admin_url()?>admin.php?page=nps_plugin%2Fincludes%2Fnps-answers-page.php&graph=1">Graphs</a></li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="setFilters">
                <h5>Filters:
        <?php
        $filterId=isset($_GET['filterId'])?$_GET['filterId']:null;
        $filterNpsId=isset($_GET['filterNpsId'])?$_GET['filterNpsId']:null;
        $filterMark=isset($_GET['filterMark'])?$_GET['filterMark']:null;
        $filterDateFrom=isset($_GET['filterDateFrom'])?$_GET['filterDateFrom']:null;
        $filterDateTo=isset($_GET['filterDateTo'])?$_GET['filterDateTo']:null;
        $searchedAnswers=\nps\models\Answers::query();
        if($filterId!==null){
            $searchedAnswers->where('id','=',$filterId);
            echo "<span class='badge badge-secondary'>id=".$filterId."</span> ";
        }
        if($filterNpsId!==null){
            $searchedAnswers->where('npsId','=',$filterNpsId);
            echo "<span class='badge badge-secondary'>npsId=".$filterNpsId."</span> ";
        }
        if($filterDateFrom!==null){
            $searchedAnswers->where('createdAt','>=',$filterDateFrom." 00:00:00");
            echo "<span class='badge badge-secondary'>mark=".$filterDateFrom."</span> ";
        }
        if($filterDateTo!==null){
            $searchedAnswers->where('createdAt',"<=",$filterDateTo." 23:59:59");
            echo "<span class='badge badge-secondary'>mark=".$filterDateTo."</span> ";
        }
        if($filterMark!==null){
            $searchedAnswers->where('mark','=',$filterMark);
            echo "<span class='badge badge-secondary'>mark=".$filterMark."</span> ";
        }
        echo "<button type='button' id='closeFilters' class='close' aria-label='Close'>
  <span aria-hidden='true'>&times;</span>
</button>";
        echo "</h5>";
        $foundAnswers=$searchedAnswers->get();

        ?>
            </div>
        </div>
        <table id="filteredAnswers" class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>npsId</th>
                <th>CreatedAt</th>
                <th>Mark</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($foundAnswers as $answer) :
                ?>
                <tr>
                    <th scope='row'><?php echo $answer->id ?></th>
                    <td><?php echo $answer->npsId ?></td>
                    <td><?php echo $answer->createdAt ?></td>
                    <td><?php echo $answer->mark ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>


    <?php
    };?>
</div>
<?php

    wp_enqueue_script( 'jquery-ui-datepicker' );
    wp_register_style( 'jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui' );




wp_enqueue_script('answersButtons',plugins_url( '/answers_buttons.js',__FILE__));
wp_enqueue_script('chartjs',plugins_url( '/node_modules/chart.js/dist/Chart.js',__FILE__));
wp_enqueue_script('graphAnswers', plugins_url( '/graphAnswersScript.js',__FILE__));
?>
</body>
</html>
