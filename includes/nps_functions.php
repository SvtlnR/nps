<?php




add_action( 'admin_menu', 'npsAddAdminLink' );


function npsAddAdminLink()
{
    add_menu_page(
        'NpsPlugin',
        'NpsPlugin',
        'manage_options',
        'nps_plugin/includes/nps-questions-page.php'
    );
    add_submenu_page( 'nps_plugin/includes/nps-questions-page.php', 'Questions', 'Questions',
        'manage_options', 'nps_plugin/includes/nps-questions-page.php');
    add_submenu_page( 'nps_plugin/includes/nps-questions-page.php', 'Answers', 'Answers',
        'manage_options', 'nps_plugin/includes/nps-answers-page.php');


}

