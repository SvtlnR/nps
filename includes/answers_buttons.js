jQuery(document).ready(function($) {
    $( function() {
        var dateFormat = "yy-mm-dd",
            from = $( "#filterDateFrom" )
                .datepicker({
                    dateFormat : "yy-mm-dd",
                    defaultDate: "+1w",
                    changeMonth: true,
                    beforeShow: function(){
                                $(".ui-datepicker").css({'font-size': '12px'});
                            }
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#filterDateTo" ).datepicker({
                dateFormat : "yy-mm-dd",
                defaultDate: "+1w",
                changeMonth: true,
                beforeShow: function(){
                            $(".ui-datepicker").css({'font-size': '12px'});
                        }
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    } );
    $("#closeFilters").click(function () {
        var url = $(".admin_url").text() + 'admin.php?page=nps_plugin%2Fincludes%2Fnps-answers-page.php';
        $.ajax({
            type: 'GET',
            data: {
                url: url,
            },
            success: function () {
                document.location.href = url;
            }
        });
    });
   $("#filter").click(function () {
        var filterId=$('#filterId').val();
        var filterNpsId=$('#filterNpsId').val();
        var filterDateFrom=$('#filterDateFrom').val();
       var filterDateTo=$('#filterDateTo').val();
        var filterMark=$('#filterMark').val();
        if(filterId!==''||filterNpsId!==''||filterDateFrom!==''||filterDateTo!==''||filterMark!=='') {
            var url = $(".admin_url").text() + 'admin.php?page=nps_plugin%2Fincludes%2Fnps-answers-page.php&filter=1';
            if(filterId!=='') url+='&filterId='+filterId;
            if(filterNpsId!=='') url+='&filterNpsId='+filterNpsId;
            if(filterDateFrom!=='') url+='&filterDateFrom='+filterDateFrom;
            if(filterDateTo!=='') url+='&filterDateTo='+filterDateTo;
            if(filterMark!=='') url+='&filterMark='+filterMark;
            $.ajax({
                type: 'GET',
                data: {
                    url: url,
                },
                success: function () {
                    document.location.href = url;
                }
            });
        }
    });
    var amountValues=new Array();
    $(".answerPerDayAmountValue").each(function(){
        var value=parseInt($(this).text());
        amountValues.push(value);
    });

    var dayLabels=new Array();
    $(".answerPerDayAmountLabel").each(function(){
        var label=$(this).text();
        dayLabels.push(label);
    });
    var marksValues=new Array();
    $(".answerMarkAmountValue").each(function(){
        var value=parseInt($(this).text());
        marksValues.push(value);
    });

    var marksLabels=new Array();
    $(".answerMarkAmountLabel").each(function(){
        var label=$(this).text();
        marksLabels.push(label);
    });



    $('#exportAmountPerDay').click( function() {
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data:{
                action: 'nps_export_amount',
                amountValues: amountValues,
                dayLabels: dayLabels
            }
        });
    });
    $('#exportMarksPerDay').click( function() {
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data:{
                action: 'nps_export_marks',
                marksValues: marksValues,
                marksLabels: marksLabels
            }
        });
    });
});
