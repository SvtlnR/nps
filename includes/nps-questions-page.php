<?php
if(isset($_POST['add_quest_title'])){
  $add_quest_title = isset($_POST['add_quest_title']) ? $_POST['add_quest_title']: null;
  $add_quest_timeout = isset($_POST['add_quest_timeout']) ? $_POST['add_quest_timeout']: null;
  $add_quest_rangemark = isset($_POST['add_quest_rangemark']) ? $_POST['add_quest_rangemark']: null;
  $add_quest_visible=isset($_POST['add_quest_visible']) ? true: false;
  $add_quest_typeview= ($_POST['add_quest_typeview']==='popup')? true:false ;
  $quests=new \nps\models\Questions();
  $quests->title=$add_quest_title;
  $quests->timeout=$add_quest_timeout;
  $quests->rangemark=$add_quest_rangemark;
  $quests->visible= $add_quest_visible;
  $quests->typeview= $add_quest_typeview;
  $quests->save();
}
?>
<style>
    .updatedRow {
    background-color:#b0bcc6;
    }
    .confirmDelete{
        background-color:  #F5F5F5;
        border-radius: 3px;
        color:  #696969;
        display: none;
        padding: 2%;
        margin: 1%;
        z-index: 99;
        position: fixed;
    }
</style>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="wrapper container">
    <div class="admin_url" style="display: none"><?php echo get_admin_url() ?></div>
    <?php if(!isset($_GET['details'])){ ?>
    <div class="showTableQuestions">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Questions</li>
                </ol>
            </nav>
        </div>
    <table id="questions" class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Timeout</th>
            <th>Rangemark</th>
            <th>Visible</th>
            <th>TypeView</th>
            <th>CreatedAt</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th></th>
            <form name="addQuestion" method="POST" action="" >
            <td><input type="text" class="form-control" name="add_quest_title" required></td>
            <td><input type="text" class="form-control" name="add_quest_timeout" size="3" required></td>
            <td><input type="text" class="form-control" name="add_quest_rangemark" size="3" required></td>
            <td><input type="checkbox" name="add_quest_visible"></td>
                <td><select class="form-control" name="add_quest_typeview">
                        <option selected value="popup">PopUp</option>
                        <option value="shortcode">Shortcode</option>
                    </select></td>
                <td></td>
            <td><input type="submit" id="add_send" value="Add" class="btn btn-outline-secondary"></td>
            <td></td>
                <td></td>
            </form>
        </tr>
        <?php
            $questions=\nps\models\Questions::query()
                ->get();
            foreach($questions as $question) : ?>
                <tr>
                    <th scope='row'><?php echo $question->id ?></th>
                    <td class='quest_title'><input type='text' class="form-control" size="10" value='<?php echo $question->title ?>' required></td>
                    <td class='quest_timeout'><input type='text' class="form-control" size="2" value='<?php echo $question->timeout ?>' required></td>
                    <td class='quest_rangemark'><input type='text' class="form-control" size="2" value='<?php echo $question->rangemark ?>' required></td>
                    <td class='quest_visible'><input type="checkbox" <?php if($question->visible) echo 'checked'?>></td>
                    <td class="quest_typeview"><select class="form-control">
                            <option <?php if($question->typeview) echo 'selected'?> value="popup">PopUp</option>
                            <option <?php if(!$question->typeview) echo 'selected'?> value="shortcode">Shortcode</option>
                        </select></td>
                    <td><?php echo $question->createdAt ?></td>
                    <td><button id='deleteQuestion' class="btn btn-outline-secondary">Delete</button></td>
                    <td><button id='updateQuestion' class="btn btn-outline-secondary">Update</button></td>
                    <td><button id='detailsQuestion' class="btn btn-outline-secondary">Details</button></td>
                    <div class="confirmDelete" id="confirmDelete_<?php echo $question->id?>">
                        <h5>Are you sure you want to delete question #<?php echo $question->id?>? </h5>
                        <div class="container my-2 feature text-center"><button  id="deleteYes_<?php echo $question->id?>" class="confirmDeleteYes btn btn-outline-secondary">Yes</button>
                        <button  id="deleteNo_<?php echo $question->id?>" class="confirmDeleteNo btn btn-outline-secondary">No</button></div>
                    </div>
                </tr>

                <?php endforeach;?>
        </tbody>
    </table>
</div>

    <?php
    }
    else{?>
    <div class="showDetails">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo get_admin_url()?>admin.php?page=nps_plugin%2Fincludes%2Fnps-questions-page.php">Questions</a></li>
                <li class="breadcrumb-item active" aria-current="page">Details</li>
            </ol>
        </nav>
        <?php
        $idQuest=isset($_GET['details']) ? $_GET['details'] : null;
            if($idQuest===null){
                echo json_encode([
                    'status' => false,
                    'line' => __LINE__,
                    'body' => [
                        'errorMessage' => 'IdQuest null'
                    ]
                ]);
                die();
            }
            $idQuest = (int)$idQuest;
            $modelQuestion = \nps\models\Questions::query()
                ->where('id', '=', $idQuest)
                ->first();
            if ($modelQuestion === null) {
                echo json_encode([
                    'status' => false,
                    'line' => __LINE__,
                    'body' => [
                        'errorMessage' => 'Nps Question Not Found'
                    ]
                ]);
                die();
            }
            $range=$modelQuestion->rangemark;
            $answers = \nps\models\Answers::query()
                ->where("npsId",'=',$idQuest)
                ->get();
            if(count($answers)<1){
                ?>
                <h5>No answers on question# <?php echo $modelQuestion->id?>:"<?php echo $modelQuestion->title?>"</h5>
                <?php
                die();
            }
            for($i=1;$i<=$range;$i++) {
                $amountMarks[$i]=0;
                foreach ($answers as $answer) {
                    if($answer->mark===$i)
                        $amountMarks[$i]++;
                }
            }
            $percent=[];
            for($i=1;$i<=$range;$i++) {
                $percent[$i]=floor($amountMarks[$i]/array_sum($amountMarks)*100*100)/100;
            }
        ?>

        <div class="showMarks">
            <div class="row">
                <div class="col">
                    <div class="titleExport"><h2>Question #<?php echo $modelQuestion->id?>: "<?php echo $modelQuestion->title?>"</h2></div>
                </div>
                <div class="col feature text-right">
                    <button id='exportData' class="btn btn-outline-secondary">Export data to CSV</button>
                </div>
            </div>
                <table class="table">
                <thead>
                <tr>
                    <th>Mark</th>
                    <td>Percent</td>
                    <td>Amount of answers</td>
                </tr>
                </thead>
                <tbody>
        <?php for($i=1;$i<=$range;$i++) :?>
                <tr>
                    <th><div class="questionMarkDetails"><?php echo $i?></div></th>
                    <td><div class="questionPercentDetails"><?php echo $percent[$i]?> %</div></td>
                    <td><div class="questionAmountDetails"><?php echo $amountMarks[$i]?></div></td>
                </tr>
    <?php
    endfor;?>
                <tr>
                    <th>
                        Total amount of answers
                    </th>
                    <td></td>
                    <td><?php echo array_sum($amountMarks)?></td>
                </tr>
                </tbody>
            </table>
            <canvas id="graphQuestion" width="400" height="400"></canvas>
        </div>
    </div>
    <?php } ?>

</div>

<?php
wp_enqueue_script('questionsButtons', plugins_url( '/questions_buttons.js',__FILE__));
wp_enqueue_script('chartjs',plugins_url( '/node_modules/chart.js/dist/Chart.js',__FILE__));
wp_enqueue_script('graphQuestion', plugins_url( '/graphQuestionScript.js',__FILE__));

?>
</body>
</html>
