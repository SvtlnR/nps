jQuery(document).ready(function($) {
    var amountValues=new Array();
    $(".questionAmountDetails").each(function(){
        var value=parseInt($(this).text());
        amountValues.push(value);
    });

    var marksLabels=new Array();
    $(".questionMarkDetails").each(function(){
        var label=$(this).text();
        marksLabels.push(label);
    });
    if(marksLabels.length>0&&amountValues.length>0) {
        var ctx = $("#graphQuestion");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: marksLabels,
                datasets: [{
                    label: 'Amount of answers',
                    data: amountValues,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1
                }]
            },
            options: {
                responsive: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }
});