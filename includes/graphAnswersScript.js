jQuery(document).ready(function($) {
    var amountValues=new Array();
    $(".answerPerDayAmountValue").each(function(){
        var value=parseInt($(this).text());
        amountValues.push(value);
    });

    var dayLabels=new Array();
    $(".answerPerDayAmountLabel").each(function(){
        var label=$(this).text();
        dayLabels.push(label);
    });
    var marksValues=new Array();
    $(".answerMarkAmountValue").each(function(){
        var value=parseInt($(this).text());
        marksValues.push(value);
    });

    var marksLabels=new Array();
    $(".answerMarkAmountLabel").each(function(){
        var label=$(this).text();
        marksLabels.push(label);
    });
    if(dayLabels.length>0&&amountValues.length>0) {
        var ctx = $("#graphAnswersAmount");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: dayLabels,
                datasets: [{
                    label: 'Amount of answers',
                    data: amountValues,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }
    if(marksLabels.length>0&&marksValues.length>0) {
            var ctx = $("#graphAnswersMarks");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: marksLabels,
                    datasets: [{
                        label: 'Amount of marks',
                        data: marksValues,
                        backgroundColor: 'rgba(54, 162, 235, 0.2)',
                        borderColor: 'rgba(54, 162, 235, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }
    // if(dayLabels.length>0&&amountValues.length>0) {
    //     var ctx = $("#graphAnswers");
    //     var myChart = new Chart(ctx, {
    //         type: 'line',
    //         data: {
    //             labels: marksLabels,
    //             datasets: [{
    //                 label: 'Amount of answers',
    //                 data: amountValues,
    //                 backgroundColor: 'rgba(255, 99, 132, 0.2)',
    //                 borderColor: 'rgba(255,99,132,1)',
    //                 borderWidth: 1
    //             },{
    //                 label: 'Amount of marks',
    //                 data: marksValues,
    //                 backgroundColor: 'rgba(54, 162, 235, 0.2)',
    //                 borderColor: 'rgba(54, 162, 235, 1)',
    //                 borderWidth: 1
    //             }]
    //         },
    //         options: {
    //             responsive: true,
    //             scales: {
    //                 yAxes: [{
    //                     ticks: {
    //                         beginAtZero: true
    //                     }
    //                 }]
    //             }
    //         }
    //     });
    // }
});